package fr.spotify.review.restservice;

    public record Error(long id, String content) { }

