package fr.spotify.review.restservice;

    public record UserInfos(long id, String content) { }

